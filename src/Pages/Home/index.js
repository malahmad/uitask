import React from "react";
import { Box, Container, Tab, Tabs } from "@mui/material";

import styles from "./Home.module.scss";

import ArrowStep from "../../Component/ArrowSteps";
import TabPanel from "../../Component/TabPanel";
import Grade1 from "../../Component/Grade1";

let steps = [
  { text: "التقييم و الاداء", selected: true },
  { text: "التقييم", selected: false },
];

export default function Homepage() {
  const [value, setValue] = React.useState(0);
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  return (
    <>
      <ArrowStep items={steps} />
      <Container maxWidth="lx" classes={{ root: styles.container }}>
        <Container maxWidth="lx">
          <Box
            sx={{ width: "100%", position: "relative" }}
            className={styles.box}
          >
            <Tabs
              onChange={handleChange}
              value={value}
              aria-label="Tabs where each tab needs to be selected manually"
              classes={{
                flexContainer: styles.tabContainer,
                indicator: styles.indicator,
              }}
            >
              <Tab
                classes={{ root: styles.tab, selected: styles.selectedTab }}
                label="الصف الاول"
              />
              <Tab
                classes={{ root: styles.tab, selected: styles.selectedTab }}
                label="الصف الثاني"
              />
              <Tab
                classes={{ root: styles.tab, selected: styles.selectedTab }}
                label="الصف الثالث"
              />
              <Tab
                classes={{ root: styles.tab, selected: styles.selectedTab }}
                label="الصف الرابع"
              />
            </Tabs>
          </Box>
          <Grade1 value={value} index={0}>
            Item One
          </Grade1>
          <TabPanel value={value} index={1}>
            Item Two
          </TabPanel>
          <TabPanel value={value} index={2}>
            Item Three
          </TabPanel>
          <TabPanel value={value} index={3}>
            Item 4
          </TabPanel>
        </Container>
      </Container>
    </>
  );
}
