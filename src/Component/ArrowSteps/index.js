import classNames from "classnames";
import React from "react";
import styles from "./ArrowSteps.module.scss";

const index = ({ items }) => {
  return (
    <div className={classNames([styles.container])}>
      <div className={classNames([styles.arrowSteps, styles.clearfix])}>
        {items?.map((element, index) => {
          return (
            <div
              className={classNames([
                styles.step,
                element.selected && styles.current,
              ])}
              key={`${index}_${element.text}`}
            >
              <span>
                <a href="#">{element.text}</a>
              </span>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default index;
