import {
  Avatar,
  Badge,
  Button,
  Card,
  CardContent,
  Container,
  FormControl,
  MenuItem,
  MobileStepper,
  OutlinedInput,
  Select,
} from '@mui/material';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import TabPanel from '../TabPanel';
import styles from './Grade1.module.scss';
import TabsUnstyled from '@mui/base/TabsUnstyled';
import { TabsList, Tab, TabPanelStyle } from '../TabPanel';
import ArrowRightIcon from '@mui/icons-material/ArrowRight';
import ArrowLeftIcon from '@mui/icons-material/ArrowLeft';
import profile from '../../assets/profileIcon.png';
import unCompleted from '../../assets/img_tumb_not_completed.svg';
import GroupRoundedIcon from '@mui/icons-material/GroupRounded';
import vedio from '../../assets/QRSCAN.mp4';

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

const Getcard = (count) => {
  return (
    <>
      {[...Array(count).keys()].map((name, index) => {
        console.log(name, index);
        return (
          <Card
            key={`item_${index}`}
            classes={{ root: styles.card }}
            sx={{ minWidth: 275 }}
          >
            <CardContent>
              <img src={unCompleted} alt="unCompleted" />
              <p>يهجى كلمات بسيطه مكونة من حرف الدال مع المدود</p>
              <span className={styles.personContainer}>
                <GroupRoundedIcon
                  classes={{ root: styles.personIcon }}
                ></GroupRoundedIcon>
                <span className={styles.number}>{`25 طالب`}</span>
              </span>
            </CardContent>
          </Card>
        );
      })}
    </>
  );
};

const Grade1 = (props) => {
  const { t } = useTranslation();

  const names1 = [t('all_grades')];
  const [personName, setPersonName] = useState([t('grade_0')]);
  const [personName1, setPersonName1] = useState([t('all_grades')]);
  const [activeStep, setActiveStep] = React.useState(0);
  const maxSteps = 46;

  const handleNext = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleChange1 = (event) => {
    const {
      target: { value },
    } = event;
    setPersonName1(
      // On autofill we get a stringified value.
      typeof value === 'string' ? value.split(',') : value
    );
  };
  const handleChange = (event) => {
    const {
      target: { value },
    } = event;
    setPersonName(
      // On autofill we get a stringified value.
      typeof value === 'string' ? value.split(',') : value
    );
  };
  return (
    <>
      <TabPanel className={styles.container} {...props}>
        <Container disableGutters classes={{ root: styles.innerContainer }}>
          <div className={styles.dropdownWrapper}>
            <div>
              <FormControl sx={{ m: 1, width: 300, mt: 3 }}>
                <Select
                  value={personName}
                  onChange={handleChange}
                  classes={{
                    icon: styles.icon,
                    select: styles.nativeInput,
                  }}
                  input={
                    <OutlinedInput
                      classes={{ notchedOutline: styles.outlined }}
                    />
                  }
                  MenuProps={MenuProps}
                  inputProps={{ 'aria-label': 'Without label' }}
                >
                  {[...Array(4).keys()].map((name, index) => {
                    return (
                      <MenuItem
                        key={t(`grade_${index}`)}
                        value={t(`grade_${index}`)}
                      >
                        {t(`grade_${index}`)}
                      </MenuItem>
                    );
                  })}
                </Select>
              </FormControl>
              <FormControl sx={{ m: 1, width: 300, mt: 3 }}>
                <Select
                  value={personName1}
                  onChange={handleChange1}
                  classes={{
                    icon: styles.icon,
                    select: styles.nativeInput,
                  }}
                  input={
                    <OutlinedInput
                      classes={{ notchedOutline: styles.outlined }}
                    />
                  }
                  MenuProps={MenuProps}
                  inputProps={{ 'aria-label': 'Without label' }}
                  renderValue={(value) => {
                    return (
                      <div className={styles.dropdownSelect}>
                        <span>{value}</span>
                        <Badge badgeContent={49} color="warning"></Badge>
                      </div>
                    );
                  }}
                >
                  {names1.map((name) => (
                    <MenuItem key={name} value={name}>
                      {name}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
            </div>
          </div>
        </Container>
      </TabPanel>
      <div className={styles.container}>
        <div className={styles.sideContainer}>
          <TabsUnstyled defaultValue={0}>
            <TabsList className={styles.subtabContainer}>
              <Tab>{t('un_comp_review')}</Tab>
              <Tab>{t('comp_review')}</Tab>
            </TabsList>
            <TabPanelStyle className={styles.tab} value={0}>
              {Getcard(6)}
            </TabPanelStyle>
            <TabPanelStyle className={styles.tab} value={1}>
              {Getcard(6)}
            </TabPanelStyle>
          </TabsUnstyled>
        </div>
        <div className={styles.mainContainer}>
          <div className={styles.profileContainer}>
            <Avatar
              src={profile}
              classes={{ root: styles.avatar }}
              sx={{ bgcolor: 'red' }}
              aria-label="recipe"
            ></Avatar>
            <MobileStepper
              variant="text"
              steps={maxSteps}
              classes={{ root: styles.stepper }}
              activeStep={activeStep}
              nextButton={
                <Button
                  size="small"
                  onClick={handleNext}
                  disabled={activeStep === maxSteps - 1}
                  classes={{ root: styles.arrowButtons }}
                >
                  <ArrowLeftIcon />
                </Button>
              }
              backButton={
                <Button
                  size="small"
                  onClick={handleBack}
                  disabled={activeStep === 0}
                  classes={{ root: styles.arrowButtons }}
                >
                  <ArrowRightIcon />
                </Button>
              }
            />
          </div>

          <div className={styles.secandaryContainer}>
            <video className={styles.player} width="500" controls>
              <source src={vedio} type="video/mp4" />
              Your browser does not support HTML video.
            </video>
            <p className={styles.desc}>
              يتعرف شكل حرف الهمزة في مواقعه المختلفة
            </p>
            <span className={styles.link}>السوال : ما هو</span>
          </div>
        </div>
      </div>
    </>
  );
};

export default Grade1;
