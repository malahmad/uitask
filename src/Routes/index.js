import { Routes, Route } from "react-router-dom";

import Home from "../Pages/Home";

export const Routing = () => {
  return (
    <div>
      <Routes>
        <Route path="Home" element={<Home />} />
      </Routes>
    </div>
  );
};
