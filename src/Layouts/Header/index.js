import { Badge, Button, Divider, Menu, MenuItem } from "@mui/material";
import classNames from "classnames";
import React, { useState } from "react";
import logo from "../../assets/Logo.svg";
import styles from "./Header.module.scss";

import AssessmentOutlinedIcon from "@mui/icons-material/AssessmentOutlined";
import LegendToggleIcon from "@mui/icons-material/LegendToggle";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";

import alert from "../../assets/ic_menu_alert.svg";
import help from "../../assets/ic_menu_help.svg";
import profile from "../../assets/ic_menu_profile.svg";
import Fade from "@mui/material/Fade";
import ScatterPlotOutlinedIcon from "@mui/icons-material/ScatterPlotOutlined";

export default function Header() {
  const [anchorEl, setAnchorEl] = useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);

  const [anchorElSubMenu, setAnchorElSubMenu] = React.useState(null);
  const openSubMenu = Boolean(anchorElSubMenu);
  const handleClickSubMenu = (event) => {
    setAnchorElSubMenu(event.currentTarget);
  };
  const handleCloseSubMenu = () => {
    setAnchorElSubMenu(null);
  };

  return (
    <div className={styles.container}>
      <img alt="Logo" className={styles.logo} src={logo} />
      <div className={styles.menuContainer}>
        <div>
          <Button className={styles.button}>
            <ScatterPlotOutlinedIcon className={styles.icon} />
            الرئيسية
          </Button>

          <Button
            id="basic-button"
            aria-controls={open ? "basic-menu" : undefined}
            aria-haspopup="true"
            aria-expanded={open ? "true" : undefined}
            onClick={handleClick}
            className={classNames([open && styles.active, styles.button])}
            endIcon={<KeyboardArrowDownIcon className={styles.arrowDown} />}
          >
            <AssessmentOutlinedIcon className={styles.icon} />
            التقييم و الاداء
          </Button>
          <Button
            endIcon={<KeyboardArrowDownIcon className={styles.arrowDown} />}
            className={styles.button}
          >
            <LegendToggleIcon className={styles.icon} />
            الانجاز
          </Button>
          <Menu
            id="basic-menu"
            anchorEl={anchorEl}
            open={open}
            onClose={handleClose}
            MenuListProps={{
              "aria-labelledby": "basic-button",
            }}
            anchorOrigin={{
              vertical: "bottom",
              horizontal: "right",
            }}
            transformOrigin={{
              vertical: "top",
              horizontal: "right",
            }}
          >
            <MenuItem onClick={handleClose}>التقييم</MenuItem>
            <Divider sx={{ my: 0.5 }} />
            <MenuItem>
              <div>
                <Button
                  id="fade-buttonSubMenu"
                  aria-controls={openSubMenu ? "fade-menuSubMenu" : undefined}
                  aria-haspopup="true"
                  aria-expanded={openSubMenu ? "true" : undefined}
                  onClick={handleClickSubMenu}
                >
                  الاداء
                </Button>
                <Menu
                  id="fade-menuSubMenu"
                  MenuListProps={{
                    "aria-labelledby": "fade-buttonSubMenu",
                  }}
                  anchorEl={anchorElSubMenu}
                  open={openSubMenu}
                  onClose={handleCloseSubMenu}
                  TransitionComponent={Fade}
                  anchorOrigin={{
                    vertical: "top",
                    horizontal: "left",
                  }}
                  transformOrigin={{
                    vertical: "top",
                    horizontal: "right",
                  }}
                  classes={{ paper: styles.mahmoud }}
                >
                  <MenuItem onClick={handleCloseSubMenu}>اداء الصف </MenuItem>
                  <Divider sx={{ my: 0.5 }} />
                  <MenuItem onClick={handleCloseSubMenu}>اداء الطالب</MenuItem>
                </Menu>
              </div>
            </MenuItem>
          </Menu>
        </div>

        <div className={styles.profileMenu}>
          <Badge classes={{ badge: styles.badge }} badgeContent={5}>
            <img alt="alert" className={styles.profileImg} src={alert} />
          </Badge>
          <img alt="help" className={styles.profileImg} src={help} />
          <Button
            classes={{ endIcon: styles.endIcon, root: styles.buttonRoot }}
            endIcon={<KeyboardArrowDownIcon className={styles.arrowDown} />}
          >
            <img alt="profile" className={styles.profileImg} src={profile} />
          </Button>
        </div>
      </div>
    </div>
  );
}
